export function setMeta(title, meta) {
    return {
        title: meta.title ? title + ' |  ' + meta.title : 'Blog cá nhân',
        meta: [
          {vmid: 'description', name: 'description', content: meta.description ? ' |  ' + meta.description : 'Đây là trang chủ: Blog của tôi'},
          {vmid: 'keywords', name: 'keywords', content: meta.keywords ? meta.keywords : 'blog,nguyenvananh'},
          {vmid: 'robots', name: 'robots', content: 'index, follow'}
        ]
      }
}