import axios from 'axios'
import { APP_CONFIG } from '@/utils/constants'

const validStatuses = [
  200, 201, 202, 203, 204,
  300, 301, 302, 303, 304
]

axios.defaults = {
  baseURL: APP_CONFIG.apiUrl,
  headers: {
    'Content-Type': 'application/json'
  },
	withCredentials: false,
	credentials: 'include',
	crossDomain: true,
}

export function processAPIErrors(apiErrors) {
  let errors = {}

  if (apiErrors) {
    for (let key of Object.keys(apiErrors)) {
      let err = apiErrors[key]

      errors[key] = err

      if (typeof err === Object || err.hasOwnProperty('length')) {
        errors[key] = apiErrors[key][0]
      }
    }
  }

  return errors
}

export const esc = encodeURIComponent

export function qs(params) {
  return (
    Object
    .keys(params)
    .map(k => esc(k) + '=' + esc(params[k]))
    .join('&')
  )
}


export function post(uri, data) {
  return axios.post(uri, data, {
    withCredentials: false,
    crossDomain: true
  })
}

export function put(uri, data) {
  return axios.put(uri, data, {
    withCredentials: false,
    crossDomain: true
  })
}

export function remove(uri) {
  return axios.delete(uri, {
    withCredentials: false,
    crossDomain: true
  })
}

export function get(uri, data = {}) {
  if (Object.keys(data).length > 0) {
    uri = `${uri}?${qs(data)}`
  }

  return axios.get(uri, {
    withCredentials: false,
    crossDomain: true
  })
}

export function upload(uri, data) {
  return fetch(uri, {
    // headers: getHeaders(true),
    // cors: true,
    method: 'PUT',
    body: data
  })
}

